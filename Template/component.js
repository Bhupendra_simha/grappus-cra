import React, { useEffect, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import * as operations from './operations';
import * as validators from './validators';
import * as constants from './constants';

import './NAME.scss';

/*
 * <NAME />
 * Author: USER (EMAIL)
 * Description: some description as what this component does
 */
const NAME = (props) => {
  // ============= props from parent =============

  // ============= props from redux =============

  // ============= constant variables =============

  // ============= useStates =============

  // ============= useRefs =============

  // ============= computed variables =============

  // ============= custom hooks =============

  // ============= functions =============

  // ============= useCallback =============

  // ============= useEffects =============

  return <div className="NAME" id="NAME"></div>;
};

NAME.propTypes = {};

NAME.defaultProps = {};

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(NAME);
