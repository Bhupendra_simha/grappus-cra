const theme = require("./theme");
const CracoAntDesignPlugin = require("craco-antd");

module.exports = {
  //FOR TAILWIND
  //   style: {
  //     postcss: {
  //           plugins: [require("tailwindcss"), require("autoprefixer")],
  //     },
  //   },
  plugins: [
    {
      plugin: CracoAntDesignPlugin,
      options: {
        customizeTheme: {
          "@primary-color": "#0077ea",
          "@link-color": "#5F36D3",
        },
      },
      // FOR TAILWIND
      //   options: {
      //     lessLoaderOptions: {
      //       lessOptions: {
      //         modifyVars: theme,
      //         javascriptEnabled: true,
      //       },
      //     },
      //   },
    },
  ],
};
