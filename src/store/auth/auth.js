const path = 'auth';

const SET_AUTH = `${path}/SET_AUTH`;

export function setAuth(payload) {
  return {
    type: SET_AUTH,
    payload,
  };
}

export const schema = {};
const initialState = schema;

// eslint-disable-next-line
export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case SET_AUTH:
      return auth(state, payload);
    default:
      return state;
  }
}

const auth = (state, payload) => {
  return {
    ...schema,
  };
};

// eslint-disable-next-line
const getAuth = () => async (dispatch) => {
  try {
    dispatch(setAuth({}));
  } catch (err) {}
};
