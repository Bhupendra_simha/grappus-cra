import React from 'react';
import PropTypes from 'prop-types';
import { Layout, Menu } from 'antd';
import logo from 'src/assets/images/logo.svg';
import './header.css';

const { Header } = Layout;
export const MainHeader = ({ menubar }) => {
  return (
    <Header className="main-header">
      <img src={logo} alt="grappus" width="100" />
      <div>
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
          {menubar.map((menu) => (
            <Menu.Item key={menu.key}>{menu.title}</Menu.Item>
          ))}
        </Menu>
      </div>
      <div className="text-white cursor-pointer">Logout</div>
    </Header>
  );
};

MainHeader.propTypes = {
  menubar: PropTypes.shape({}),
};
