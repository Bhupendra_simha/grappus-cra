import React from 'react';

import { Layout } from 'antd';

import MainHeader from 'src/common/Header';
import HeaderData from 'src/constants/headerData';
import logo from 'src/assets/images/logo.svg';
import './homepage.scss';

const { Content } = Layout;

function HomePage() {
  return (
    <Layout>
      <MainHeader menubar={HeaderData} />
      <Content className="bg-gray">
        <div style={{ padding: 29, minHeight: '150vh' }}>
          <div className=" flex flex-col items-center justify-center">
            <img
              className=" bg-black p-30 mt-4  "
              src={logo}
              alt="grappus"
              width="500"
            />
          </div>
          <div className="flex flex-col items-center justify-center mt-20">
            <div className="text-20  mt-20">base react repo... 👨🏻‍💻</div>
            <div className="text-20 w-500 mt-20">create a kickass project</div>
            <div className="text-grey text-20 mt-20">
              Authors: Front End Team
            </div>
          </div>
        </div>
      </Content>
    </Layout>
  );
}

export default HomePage;
