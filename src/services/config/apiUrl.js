const development = {
  API_URL: process.env.REACT_APP_API_BASE_ENDPOINT,
  SEARCH_URL: process.env.REACT_APP_API_SEARCH_ENDPOINT,
};

const production = {
  API_URL: "",
};

export default development;
