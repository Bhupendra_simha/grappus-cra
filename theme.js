module.exports = {
    '@font-family': `Nunito, sans-serif`,
    '@modal-header-padding-vertical': '@padding-lg',
    '@modal-header-padding-horizontal': '@padding-lg',
    '@layout-header-background': '#000000',
    '@text-color-secondary': '#000000',
    '@primary-color': '#333333',
    '@processing-color': '#333333',
    '@success-color': 'hsl(100, 77%, 44%)',
    '@warning-color': '#faad14',
    '@error-color': '#f5222d',
    '@font-size-base': '14px',
    '@border-radius-base': '4px',
    '@border-color-base': '#d9d9d9',
  };
  